
var speciesLib = function() {
	var p = {
		version : 'species lib v. 1.43',
		f : 'species lib: ',
		debug : false,
		delayMillis : 500,
		deleteControl : '.SectionDeleteControl',
		speciesContainer : '[sectionid="Sspecies"].EftRepeatingSection.NotHidden > div',
		speciesSection : '[sectionid="Sspecies"].EftRepeatingSection.NotHidden > div > div[htmltag="div"]',
		speciesTotal : '[elementid="QspeciesTotal"] input.NotHidden',
		numAdditionalAnimals : '[elementid="QnumExtraAnimals"] input.NotHidden',
		groupSection : '[sectionid="SspeciesGroup"].EftRepeatingSection.NotHidden > div > div[htmltag="div"]',
		
		// Metagroup total
		groupQuantity : '[elementid="QgroupQty"] input.NotHidden',
		
		// Summarize experimental groups table
		tableField : '[elementid="QgroupTable"] input.NotHidden',
		
		// the table sectionID is assigned dynamically
		tableRow : '[elementid="QgroupTable"] div[sectionid].TableQuestion tr[htmltag="tr"]',
		
		// Number of groups
		tableField1 : '[elementid="QnumGroups"] input.NotHidden',
		
		// Number per group
		tableField2 : '[elementid="QnumPerGroup"] input.NotHidden',
		
		// Number of  years
		tableField3 : '[elementid="QnumYears"] input.NotHidden',
		
		proceduresSection : '[sectionid="Sprocs"].EftRepeatingSection.NotHidden > div > div[htmltag="div"]',
		
		// the procedure names we want to collect
		procedures : '[elementid="QprocName"] .PickListQuestion.NotHidden span',
		
		// the procedures to be checked
		procedureCheckboxes : '[sectionid="S3"] [elementid^="cb"]',
		procedureCacheLifetimeMinutes : 25,
		//
		totalSpecies : false, 
		collectProcedures : false,
		setProcedures : false,
	}

	var delay = (function() {
		var timer = 0;
		return function(callback, ms) {
			clearTimeout(timer);
			timer = setTimeout(callback, ms);
		};
	})();
	
	function urlParam(name, url, defaultValue) {

		if (typeof url == 'undefined' || url == null) {
			url = window.location.href;
		}
		// remove extension if from local file
		url = url.replace(/\.html$/,'');

		var m = url.match(/.+?\('(.+)'\)/);
		if (m && m.length > 0) {
			url = m[1];
		}

		var s1 = '[\\?&]' + name + '=([^&#]*)';
		var r = new RegExp(s1); 
		
		var results = url.match(r);
		if (results == null) {
			r = new RegExp(s1, 'i');
			results = url.match(r);
		}
		if (results == null) {
			defaultValue = (typeof defaultValue === 'undefined') ? null : defaultValue;
			return defaultValue;
		} else {
			return results[1] || 0;
		}
	}
	
	function removeOnChangeEvent(cntx) {
		jQuery('input', cntx).removeAttr('onchange');
	}
	
	function restoreOnChangeEvent(cntx) {
		
		jQuery(cntx).on('change', 'input', function (e) {
			var o = jQuery(this);
			var val = o.val();
			var jso = o.get(0).jsObject;
			
			if (p.debug) {
				console.group(p.f + 'On change event');
				console.log('input element class is ' + o.get(0).className);
			}
			
			if (typeof jso !== 'undefined') {
				if (p.debug) {
					console.log('input element has jsObject - applying original onchange code');
				}

				var newval;
				try {
					newval = jso.ProcessInput(val);
					if (newval != val) {
						jso.SetInputValue(newval);
						val = newval;
					}
				}
				catch (err) {
					jso.SetInputValue(jso.Value);
					return;
				}
				jso.set_Value(jso.Hidden ? '' : val);
			}
			
			if (p.debug) {
				console.groupEnd();
			}

		});
	}
	
	function save() {
		if (p.debug) {
			console.group(p.f+'Saving');
			console.groupEnd();
		}
		jQuery('.fm-group-total-1').get(0).jsObject.AutoSave();
	}
	
	
	function change(jqObject, val, triggerChange) {
		jqObject.val(val);
		if (triggerChange) {
			jqObject.change();
			jqObject.get(0).dispatchEvent(new Event('change', { 'bubbles': false }));
		}
	}

	function totalSpecies() {
		
		// for each species
		jQuery(p.speciesSection).each(function (i,e) {
			var sp = jQuery(this);
			
			var speciesTotal = sp.find(p.speciesTotal);
			speciesTotal.addClass('fm-species-total-'+i);
			var specTotal = 0;

			if (p.debug) {
				console.group(p.f+'totaling species groups');
				console.log('Species '+i);
			}
			
			// for each group
			jQuery(p.groupSection, sp).each(function (j,e) {
				var gp = jQuery(this);
				
				var groupClass = 'fm-group-'+j;
				gp.addClass(groupClass);

				if (p.debug) {
					console.log('Species '+i+' Group '+j);
				}
				
				var groupQ = 0;
				var groupQuantity = gp.find(p.groupQuantity);
				groupQuantity.addClass(groupClass + ' ' + 'fm-group-total-'+j);
				
				gp.find(p.tableRow).each(function (k,e) {
					var row = jQuery(this);
					var v1 = row.find(p.tableField1);
					var v2 = row.find(p.tableField2);
					var v3 = row.find(p.tableField3);
					v1.addClass(groupClass + ' ' + 'fm-val-1');
					v2.addClass(groupClass + ' ' + 'fm-val-2');
					v3.addClass(groupClass + ' ' + 'fm-val-3');

					if (p.debug) {
						console.log('Species '+i+' Group '+j+' table row '+k);
					}
					
					groupQ += (v1.val() * v2.val() * v3.val());
				});

				change(groupQuantity, groupQ, true);

				// num additional animals
				var numExtraAnimals = sp.find(p.numExtraAnimals);
				numExtraAnimals.addClass('fm-species-num-exta-animals');
				var numExtra = numExtraAnimals.val();
				
				specTotal+= numExtra + groupQ;
				
				if (p.debug) {
					console.groupEnd();
				}
				
			});

			change(speciesTotal, specTotal, true);
		});
		
	    delay(function () {
           //save();
        }, p.delayMillis);
	}

	function getNumDeleteControls() {
		return jQuery(p.deleteControl).length;
	}
	var numDeleteControls = getNumDeleteControls();
	
	
	function getNumTableRows() {
		return jQuery(p.tableRow).length;
	}
	
	var numTableRows = getNumTableRows();
	
	function getNumProcedures() {
		return jQuery(p.procedures).length;
	}
	var numProcedures = getNumProcedures();
	
	var recordID = urlParam('recordid');
	var lsKey_procedures = 'speciesLib.procedures.'+recordID;
	
	function collectProcedures() {
		var list = jQuery(p.procedures).toArray().map(function(item) { return item.innerText.trim(); }) || [];
		var data = {
				procedures:list,
				time:(new Date()).getTime(),
				expired:false,
		}
		window.localStorage.setItem(lsKey_procedures, JSON.stringify(data));
		
		if (p.debug) {
			console.group(p.f+'collecting procedures');
			console.log('procedures',data);
			console.groupEnd();
		}
	}
	function getProcedures() {
		var data = {
				procedures:[],
				time: (new Date()).getTime(),
				expired:false,
		}
		if (p.debug) {
			console.group(p.f+'getProcedures ' + lsKey_procedures);
		}
		var proc = JSON.parse(window.localStorage.getItem(lsKey_procedures)) || data;
		 
		var seconds = (new Date()).getTime() - proc.time;
		
		var expired = (seconds >= p.procedureCacheLifetimeMinutes*60*1000); 
		proc.expired = expired;

		if (p.debug) {
			console.log(seconds, p.procedureCacheLifetimeMinutes*60*1000);
			console.log('Seconds', seconds, proc);
			console.groupEnd();
		}
		
		return proc;
	}
	
	function setProcedures(proc) {
		var list = proc.procedures;
		if (list.length>0) {
			jQuery(p.procedureCheckboxes).each(function (i) {
				var o = jQuery(this);
				var t = o.find('span.QuestionLabel span').text();
				if (list.includes(t)) {
					if (p.debug) {
						console.log('setProcedures: Checking box for ' + t);
					}
					var obj = o.find('.YesNoQuestion.NotHidden[title="Item Not Checked"]').get(0);
					if (obj) { obj.click();	}
				} else {
					var obj = o.find('.YesNoQuestion.NotHidden[title="Item Checked"]').get(0);
					if (obj) { obj.click();	}
				}
			});
		}
	}

	
	function activate(opt) {
		var lib = this;
		if (typeof opt === 'object') {
		      jQuery.extend(p, opt);
		}
		
		if (p.totalSpecies) {
			// remove change handler since it autosaves
			removeOnChangeEvent(p.speciesContainer);

			// set new change handler for current and future inputs
			restoreOnChangeEvent(p.speciesContainer);
			
			// when table numbers change
			jQuery(p.speciesContainer).on('change', p.tableField, function () {
				totalSpecies();
			});
		}
		if (p.collectProcedures) {
			collectProcedures();
		}
		if (p.setProcedures) {
			var proc = getProcedures();
			setProcedures(proc);
		}
			
		// when page changes
		MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
		var observer = new MutationObserver(function(mutations, observer) {
			if (p.debug) {
				console.group(p.f+'DOM changed');
				mutations.forEach(function(mutation) { console.log('Change type is '+ mutation.type); });
			}
			
			if (p.totalSpecies) {
				var dels = getNumDeleteControls();
				if (dels !== numDeleteControls) {
					if (p.debug) {
						console.log('Num delete controls changed! - on change events will be removed');
						console.log('There are ' + dels + ' delete controls');
					}
					if (dels > numDeleteControls) {
						removeOnChangeEvent(p.speciesContainer);	
					}
					numDeleteControls = dels;
				}
				
			    var rows = getNumTableRows();
			    if (rows !== numTableRows) {
			    	if (p.debug) {
			    		console.log('Num table rows changed! - species group will be totaled');
			    		console.log('There are ' + rows + ' table rows');
			    	}
			    	numTableRows = rows;
			    	
			    	totalSpecies();	
			    }
			}
			if (p.collectProcedures) {
				var numProcs = getNumProcedures();
				if (numProcs != numProcedures) {
					if (p.debug) {
			    		console.log('Num procedures has changed! - procedures will be collected');
			    		console.log('There are ' + numProcs + ' procedures');
			    	}
					collectProcedures();
					numProcedures = numProcs;
				}

			}
			
		    if (p.debug) {
		    	console.groupEnd();
		    }
		});
		observer.observe(document, {
		  subtree: true,
		  attributes: false,
		  childList : true
		});
		
		
		console.group(p.f+'activate');
		console.log('settings', p);
		console.groupEnd();
	}
	
	
	console.log(p.version);
	
	return {
		activate : activate,
	}
	
}();

// for species mapped page
speciesLib.activate({debug:true, totalSpecies:true, collectProcedures:true});

// for procedures page
//speciesLib.activate({debug:true, setProcedures:true});


