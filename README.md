# Form math

Add up some values into sub and grand totals on your form

## Species form

The species-form-math.js file contains a custom example of the intent of this project. This could be used to build from specific to generic.

### Context

In a popular electronic research administration program, the form builder for the animal studies component allows you to create a section for recording species data. The species form is extendable and so we have added totals for the animals used. Rather than expect the user to fill out the totals, we want to automate it with javascript.

The format of our tweaked species form looks like:

Species A

Species A total : __________

Species A Number of additional animals (for training): ____________

Group A1

Group A1 total : __________

Experimental group A1 table:

| Name | Number of groups | Number per group | Number of years |
| ---- | ---------------- | ---------------- | --------------- |
|      |                  |                  |                 |
|      |                  |                  |                 |

Group A2

Group A2 total : __________

Experimental group A2 table:

| Name | Number of groups | Number per group | Number of years |
| ---- | ---------------- | ---------------- | --------------- |
|      |                  |                  |                 |

Species B

Species B total : __________

Group B1

Group B1 total : __________

Experimental group B1 table:

| Name | Number of groups | Number per group | Number of years |
| ---- | ---------------- | ---------------- | --------------- |
|      |                  |                  |                 |

The table rows are multiplied and summed into the group totals, which are summed along with animal additional training animals into species totals. We don't have or need a grand total.

The form builder allows us to include javascript and use jQuery. The species form adds and removes table rows using javascript so page reloads don't occur.

## Extensions

As we were working on this, the requirment arose to collect procedures from the species form and use them to check boxes on the procedures form so the investigator would not have to do this. Rather than build a separate solution since the framework was already here, we used this code to achieve that goal. Which action is performed is determined by setting boolean values on the library object. The actions are

* totalSpecies
* collectProcedures
* setProcedures

Ex. 

```javascript
// for species mapped page
speciesLib.activate({debug:true, totalSpecies:true, collectProcedures:true});

// for procedures page
speciesLib.activate({debug:true, setProcedures:true});
```

### Challenges

The HTML produced by the form builder is complex. Identifying repeating sections takes some time, also rebuilding the form assigns new section IDs. It is best to set question and section IDs manually in the form designer.

Updating the total when you change a group count is straightforward using the change event. Handling removal of rows and of sections is harder. We need to update totals when a table row is removed and when an entire group is removed. The solution for this is to use a MutationObserver.

The form autosaves via an onChange event. This prevents updating values on our own change event because the autosave also refreshes input values. To do anything with the change event, autosave must first be disabled. This is accomplished by removing the onChange attribute. This must be done for existing and new content. The same MutationObserver is also used to clear onChange for new content.


